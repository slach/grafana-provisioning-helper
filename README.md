# grafana-provisioning-helper

Since graphan developers cannot implement auto-enable for installed applications and good IaaC, I implemented simple monkeypatch.

## quickstart

    wget -O artifacts.zip https://gitlab.com/egeneralov/grafana-provisioning-helper/-/jobs/artifacts/master/download?job=build-python
    export BASE_URL="https://grafana.example.com"
    export USERNAME=egeneralov
    export PASSWORD=egeneralov
    ./grafana-provisioning-helper

## What will happens

- all plugins will be enabled
- all datasources will be enabled
  - for [devopsprodidy-kubegraf-datasource](https://grafana.com/grafana/plugins/devopsprodigy-kubegraf-app)
    - enabled:
      - tlsAuthWithCACert
      - tlsAuth
    - rewrited:
      - cluster_url = datasource url
      - refresh_pods_rate = 15
    - disabled:
      - tlsSkipVerify
  - for [grafana-kubernetes-datasource](https://grafana.com/grafana/plugins/grafana-kubernetes-app) will be
    - enabled:
      - tlsAuthWithCACert
      - tlsAuth
    - disabled:
      - tlsSkipVerify

## Feature Request

Just open an issue or send an email to [eduard@generalov.net](mailto:eduard@generalov.net)
